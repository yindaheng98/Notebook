# 托福口语 Part 2 逐字稿练习记录

#### Official 50 Speaking 3

The men agree with the opinion since

At first, he thinks this is a waste of money. He thinks the money should be used for more valuable projects. He mentioned a library project that stuck for a long time, which disappointed him and his friends.

Second, he thinks this is not good for the sky environment. It will bother the telescope that sees the star in the sky. This is related to an astronomy project, without this they can only see the stars in the textbook.

#### Official 66 Speaking 2

##### 范例

In the reading, the student proposes that the university should prohibit students from playing sports on the dining hall lawn because this will make the grass look nicer and instead, students can use the field next to the gym as a substitute. In the listening, the woman totally disagrees with this proposal because she thinks that the students who play sports aren’t the ones who are responsible. In fact, everybody crosses the lawn every day since it is centrally located. And they don't even bother walking on the sidewalks because usually people are in a hurry to get to class. On top of that, the substitute is not very good since in order to use that, students need to walk for a long distance. But actually, sometimes they just want to have a little fun in between classes. So they will not bother going that far.

##### 踩分点

* Reading
  * the university should ban sports on the grass in front of the hall
    * make the grass look nicer
  * use the field next to the gym for sport
* Listening
  * ban sports does not make sense
    * import way in university
    * lots of people go through there every day
    * ban sports cannot make things better
  * gym is inconvient
    * far away
    * people just want little fun between class

#### Official 63 Speaking 2

##### 范例

In the reading, the University announces that students who are going to graduate will be able to use a new degree Navigator website to keep track of their requirements because this will make it easier for them to check the progress online and at the same time reduce the workload for academic advisers. In the listening, the man totally agrees with the announcement, because he says that students need to deal with a lot of classes for degree programs and it's easy for them to make a mess. Sometimes they even miss particular courses just like his friend whose graduation was delayed just because he didn't realize that he hadn't finished his history degree. Also, he thinks that the advisers who are mostly professors are really busy working, so this website will free them from redundant tasks and have more time to focus on serious things, like switching a major or doing internships.

##### 踩分点

* Reading
  * new website
    * keep track of their course
  * easier for them to check the progress
  * reduce the workload for academic advisers
* Listening
  * miss course
    * lots of courses
    * difficult to organize
    * example: friend miss a course and graduation was delayed
  * advisers who are mostly professors
    * busy working
    * free them from redundant tasks
    * more time to focus on serious things
      * switching a major
      * doing internships
