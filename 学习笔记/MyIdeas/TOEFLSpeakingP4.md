# 托福口语 Part 4 逐字稿练习记录

#### Official 53 Speaking 6

##### 初版

The professor introduced two ways to prevent food from spoil. 

The first is about temperature. Foods get spoiled mainly because of bacteria. When the temperature is low, bacteria will grow slower. The professor takes fish as the example. When fish is frozen, it will last long, because bacteria grow slowly in low temperature. 

The second is about moisture. Moisture is important to the growth of bacteria, bacteria cannot grow without it. The professor takes milk as example. Power milk last longer than liquid milk. Since the moisture is removed, bacteria cannot grow.

#### Official 44 Speaking 6

The professor introduced two ways that animals can benefit from forest fires.

First is that forest fires help predators to find food