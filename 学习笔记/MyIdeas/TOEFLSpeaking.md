# 托福口语

## 考试感觉记录

考试次数|分数|考试感觉
-|-|-
1|18|首考
2|22|初步练习，流畅了一些，但结尾剩余时间较多，靠后的采分点说的少
3|19|进一步练习，更加流畅，细节更多，但靠后的采分点说的少，有个答案甚至被截断
4|19|进一步练习，有个Part把South说成了North，有一个Part多说了一个to就被截断

## 考试重点技巧记录

**只讲重点！只讲重点！只讲重点！不重点的细节别TM讲，浪费时间**

主要讲**因果**和**对照对比**之类的**逻辑**

**答案千万不能被截断**

**准备时间对着笔记从头到尾组织一下语言**

### Part 1

![](./TOEFL/Speaking.png)

逻辑三段式：
1. 结论
2. 细节/例证
3. 结论

**逻辑要求没有非常高，主要是能说**

### Part 2

阅读：
* 什么发生了变化
* 原因1核心
* 原因2核心

#### 答题模板

According to the announcement / proposal / letter ...

The student (does not) think this is a great idea for two reasons.

First, he/she mentions that ... Because ... Therefore, ...

Besides, he/she also mentions that ... Because ... As a result, ...

![](./TOEFL/SpeakingT2.png)

### Part 3

**开头概述文章内容只讲核心！别讲细节！浪费时间**

**主要讲听力内容！**

文本内容只记下概念定义用于讲解，其他的都只用来辅助理解

一篇阅读+听力里两个例子

初学起步120词，最多不能超过150词

![](./TOEFL/SpeakingP3.png)

The reading talks about the term of ..., which refers to ...

First, he/she mentions that ... For example

Second, he/she also mentions that

![](./TOEFL/SpeakingT3.png)

#### 记笔记

记逻辑，逻辑只有两种重要：对比/因果

重点听因果和逻辑

避免3秒以上停顿

## Part 4

In the lecture, the professor talks about xxx, which is ...

First, the professor discusses...

Second, he/she further discusses ...

![](./TOEFL/SpeakingT3.png)
