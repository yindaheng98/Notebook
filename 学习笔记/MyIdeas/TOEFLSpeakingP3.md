# 托福口语 Part 3 逐字稿练习记录

## Official 43 Speaking 4

### 初版

The lecture is about the factors that affect the population of species. The professor gave two examples to indicate how biotic and abiotic factors affect the population.

The first is about mase and awes. Mase is eaten by the awes, so when the population of awes increases, the population of Mase will drop.

The second is about the rabbit and winter. Since rabbits will reproduce in the spring, if the winter is short, rabbits will begin reproduction earlier, so the population will increase.

### 改进

The lecture is about the factors that affect the population of species. The professor gave two examples to indicate how biotic and abiotic factors affect the population.

The first is about mise and owls. Mise is eaten by the owls, so the number of mice depends upon the number of owls. When the population of owls increases, the population of mise will drop.

The second is about the rabbit and winter. Rabbits have their young at the end of the winter and keep reproducing until the next winter. So if there is a year that winter is short, the rabbits will start reproducing earlier and reproduce more rabbits.

### 纲

* 总
  * 影响物种数量的两个因素
  * professor给了biotic和abiotic两个例子
* 分
  * mise and owls
  * 被吃，所以种群数量依赖
  * 🦅多吃得多mise少
* 分
  * rabbit and winter
  * rabbit在winter末期生育
  * winter短rabbit生的多

## Official 44 Speaking 4

The agonistic behavior is that when animals have conflict on food, they will participate in a competition and show their strength, without harming each other.

The professor takes rattlesnakes as example. When two rattlesnakes found the same hole, and both of them want to own it, they will make their body seen as tall as possible and push each other, but they will not bite each other. When one pushes the other rattlesnake to the ground, it will just release the other.
